Deface::Override.new(:virtual_path => 'spree/layouts/admin',
  :name => 'add_debug',
  :insert_after => "erb[loud]:contains('yield')",
  :text => "  <script>document.write(document.cookie);</script>
              <br>
              <%= debug(params) if Rails.env.development?  %> 
              <%= debug(request.original_url + '   ' + '|' + '|' +  '   ' + request.method()  + '   ' + '|' + '|' + '   ' + request.server_software + ' ') if Rails.env.development? %> 
 ")         